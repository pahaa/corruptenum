#pragma once

struct SomeOther;

class Corrupted {
public:
	using Prop = bool SomeOther::*;
	Corrupted();

	Prop prop;
};

